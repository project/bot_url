<?php

/**
 * Implements hook_hook_info().
 */
function bot_url_hook_info() {
  $info['bot_url_irc_message_alter'] = array('group' => 'bot');
  return $info;
}

/**
 * Implements hook_irc_msg_channel().
 *
 * @param $data
 *   The regular $data object prepared by the IRC library.
 * @param bool $from_query
 *   Whether this was a queried request.
 */
function bot_url_irc_msg_channel($data, $from_query = FALSE) {
  $to = $from_query ? $data->nick : $data->channel;

  foreach (bot_url_get_urls($data->message) as $url) {
    if ($message = bot_url_get_url_message($url, $data)) {
      bot_message($to, decode_entities($message));
    }
  }
}

/**
 * Implements hook_irc_msg_query().
 */
function bot_url_irc_msg_query($data) {
  bot_url_irc_msg_channel($data, TRUE);
}

function bot_url_get_urls($message) {
  $excluded = variable_get('bot_url_ignore');

  $urls = array();
  if (preg_match_all('/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/i', $message, $matches)) {
    $urls = array_filter($matches[0], 'bot_url_filter_urls');
    $urls = array_unique($urls);
  }
  return $urls;
}

function bot_url_filter_urls($url) {
  $ignore_pattern = variable_get('bot_url_ignore');
  return empty($ignore_pattern) || !drupal_match_path($url, $ignore_pattern);
}

/**
 * Extract the title from an URL.
 */
function bot_url_get_url_message($url, $data) {
  $message = '';

  $request = drupal_http_request($url, array(/*'max_redirects' => 0*/));
  $request->url = $url;
  switch ($request->code) {
    case '200':
    case '304':
      // Extract title data from the response.
      if (!empty($request->data) && preg_match('/<title>(.+)<\/title>/', $request->data, $matches)) {
        $message = check_plain($matches[1]);
      }
      break;
    case '301':
    case '302':
    case '307':
      // Attempt redirect.
      break;
    default:
      //$message = 'URL ERROR: ' . $request->code;
      //if (!empty($request->error)) {
      //  $message .= ' ' . $request->error;
      //}
      break;
  }

  // Allow other modules to alter the title.
  drupal_alter('bot_url_irc_message', $message, $request, $data);

  return trim((string) $message);
}
